import { Route, Routes } from 'react-router-dom';

import Tasks from '../pages/Tasks';
import TaskDetail from '../pages/Tasks/Detail';
import TaskForm from '../pages/Tasks/Form';

export const MyRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Tasks />} />
      <Route path="/nova_tarefa" element={<TaskForm />} />
      <Route path="/editar_tarefa/:id" element={<TaskForm />} />
      <Route path="/detalhar_tarefa/:id" element={<TaskDetail />} />
    </Routes>
  );
};
