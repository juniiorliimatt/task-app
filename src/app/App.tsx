import { BrowserRouter } from 'react-router-dom';
import Header from './components/Header';
import { MyRoutes } from './routes/routes';

export const App = () => {
  return (
    <BrowserRouter>
      <Header />
      <MyRoutes />
    </BrowserRouter>
  );
};
