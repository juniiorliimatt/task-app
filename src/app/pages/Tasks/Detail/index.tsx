import '../style.css';

import moment from 'moment';
import { useEffect, useState } from 'react';
import { Badge, Button, Card } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';

import { Task } from '../../../shared/models/Task';
import api from '../../../shared/services/api';

const TaskDetail = () => {
  const navigate = useNavigate();
  const [task, setTask] = useState<Task>({} as Task);
  const { id } = useParams();

  useEffect(() => {
    loadTask();
  }, []);

  async function loadTask() {
    const response = await api.get(`/${id}`);
    setTask(response.data);
  }

  function formatDate(date: Date) {
    return moment(date).format('DD/MM/YYYY HH:mm:ss');
  }

  function back() {
    navigate('/');
  }

  return (
    <div className="container">
      <div className="task-header">
        <div>
          <h1>Detalhe da Tarefa</h1>
        </div>
        <div>
          <Button variant="dark" size="sm" onClick={back}>
            voltar
          </Button>
        </div>
      </div>
      <div className="container">
        <Card>
          <Card.Body>
            <div>
              <Card.Text>
                <Badge bg={task.finished ? 'success' : 'warning'} text="dark">
                  {task.finished ? 'FINALIZADA' : 'PENDENTE'}
                </Badge>{' '}
                Criado em:{' '}
                <Badge bg="info" text="dark">
                  {formatDate(task.createdAt)}
                </Badge>{' '}
                Atualizado em:{' '}
                <Badge bg="info" text="dark">
                  {formatDate(task.updatedAt)}
                </Badge>
              </Card.Text>
              <div>
                <Card.Title>{task.tittle}</Card.Title>
              </div>
              <div>
                <Card.Subtitle>{task.description}</Card.Subtitle>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>
    </div>
  );
};

export default TaskDetail;
