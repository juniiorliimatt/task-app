import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import { Task } from '../../../shared/models/Task';
import api from '../../../shared/services/api';

import './style.css';

const TaskForm = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [task, setTask] = useState<Task>({
    id: '',
    tittle: '',
    description: '',
    finished: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  });

  useEffect(() => {
    if (id !== undefined) {
      findTask();
    }
  }, [id]);

  function back() {
    navigate('/');
  }

  function updateTask(e: ChangeEvent<HTMLInputElement>) {
    setTask({
      ...task,
      [e.target.name]: e.target.value,
    });
  }

  async function onSubmit(e: ChangeEvent<HTMLFormElement>) {
    e.preventDefault();

    if (id !== undefined) await api.put(`/${id}`, task);
    if (id === undefined) await api.post('/', task);

    navigate('/');
  }

  async function findTask() {
    const response = await api.get<Task>(`/${id}`);
    setTask({
      id: response.data.id,
      tittle: response.data.tittle,
      description: response.data.description,
      finished: response.data.finished,
      createdAt: response.data.createdAt,
      updatedAt: response.data.updatedAt,
    });
  }

  return (
    <div className="container">
      <div className="task-header">
        <div>
          <h1>Nova Tarefa</h1>
        </div>
        <div>
          <Button variant="dark" size="sm" onClick={back}>
            Voltar
          </Button>
        </div>
      </div>

      <Form onSubmit={onSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Titulo</Form.Label>
          <Form.Control
            value={task.tittle}
            name="tittle"
            type="text"
            placeholder="Título da tarefa"
            onChange={(e: ChangeEvent<HTMLInputElement>) => updateTask(e)}
          />
          <Form.Text className="text-muted">
            Um titulo para sua tarefa, de forma resumida.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Descrição</Form.Label>
          <Form.Control
            as="textarea"
            value={task.description}
            name="description"
            placeholder="Descrição da sua tarefa"
            onChange={(e: ChangeEvent<HTMLInputElement>) => updateTask(e)}
          />
          <Form.Text className="text-muted">
            Descreva como será sua tarefa.
          </Form.Text>
        </Form.Group>
        <Button variant="dark" type="submit">
          Salvar
        </Button>
      </Form>
    </div>
  );
};

export default TaskForm;
