import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Badge, Button, Table } from 'react-bootstrap';
import { Task } from '../../shared/models/Task';

import api from '../../shared/services/api';
import './style.css';
import moment from 'moment';

const Tasks = () => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const navigate = useNavigate();

  useEffect(() => {
    loadTasks();
  }, []);

  async function loadTasks() {
    const response = await api.get('/');
    setTasks(response.data);
  }

  function formatDate(date: Date) {
    return moment(date).format('DD/MM/YYYY HH:mm:ss');
  }

  function newTask() {
    navigate('/nova_tarefa');
  }

  function editTask(id: string) {
    navigate(`/editar_tarefa/${id}`);
  }

  function detailTask(id: string) {
    navigate(`/detalhar_tarefa/${id}`);
  }

  async function finishedTask(id: string) {
    await api.patch(`/${id}`);
    loadTasks();
  }

  async function deleteTask(id: string) {
    await api.delete(`/${id}`);
    loadTasks();
  }

  return (
    <div className="container">
      <div className="task-header">
        <div>
          <h1>Task Page</h1>
        </div>
        <div>
          <Button variant="dark" size="sm" onClick={newTask}>
            Nova Tarefa
          </Button>
        </div>
      </div>
      <Table className="text-center" striped bordered hover>
        <thead>
          <tr>
            <th hidden>ID</th>
            <th>Titulo</th>
            <th>Data de Atualização</th>
            <th>Status</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          {tasks.map((task) => (
            <tr key={task.id}>
              <td hidden>{task.id}</td>
              <td>{task.tittle}</td>
              <td>{formatDate(task.updatedAt)}</td>
              <td>
                <Badge bg={task.finished ? 'success' : 'warning'} text="dark">
                  {task.finished ? 'FINALIZADA' : 'PENDENTE'}
                </Badge>{' '}
              </td>
              <td>
                <Button
                  disabled={task.finished}
                  size="sm"
                  variant="primary"
                  onClick={() => {
                    editTask(task.id);
                  }}
                >
                  Editar
                </Button>{' '}
                <Button
                  disabled={task.finished}
                  size="sm"
                  variant="success"
                  onClick={() => {
                    finishedTask(task.id);
                  }}
                >
                  Finalizar
                </Button>{' '}
                <Button
                  size="sm"
                  variant="secondary"
                  onClick={() => {
                    detailTask(task.id);
                  }}
                >
                  Visualizar
                </Button>{' '}
                <Button
                  size="sm"
                  variant="danger"
                  onClick={() => {
                    deleteTask(task.id);
                  }}
                >
                  Remover
                </Button>{' '}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Tasks;
