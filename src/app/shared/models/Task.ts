export interface Task {
  id: string;
  tittle: string;
  description: string;
  finished: boolean;
  createdAt: Date;
  updatedAt: Date;
}
